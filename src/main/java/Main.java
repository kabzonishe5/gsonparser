import com.google.gson.Gson;
import org.apache.commons.math3.util.Precision;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("����� ���������� � GydronBank Corporation");
        Scanner scanner = new Scanner(System.in);
        GsonParser gsonParser = new GsonParser();
        CurrencyList course = gsonParser.readUrl2();
        ConverterList converterList = new ConverterList();
        course.setCourse(course.getValute());
        BankAccount bankAccount = new BankAccount();
        bankAccount.createAccount();
        while (true) {
            if (!bankAccount.isLockAccount()) {
                System.out.println("������ ���������� ������ ���������:");
                System.out.println("1. ������������� ������� ������.");
                System.out.println("2. ��������� ���� �� �������� ������.");
                System.out.println("3. ������� �������� ����.");
                System.out.println("4. ������� ���� ������ ������.");
                System.out.println("5. ������� ���� �� �������� ������.");
                System.out.println("6. �������� ������ � ������ �������� ������.");
                int choice = Integer.parseInt(scanner.nextLine());
                if (choice == 1) {
                    bankAccount.setLockAccount(true);
                    System.out.println("���� ������� ������ �������������.");
                } else if (choice == 2) {
                    if (bankAccount.getBalanceList().isEmpty()) {
                        System.out.println("� ��� ��� ��������� �����, ��������� � ���� ����� ������� ���.");
                    } else {
                        bankAccount.getBalanceList().forEach((key, value) -> System.out.println(key + "=" + value.getMoney()));
                        System.out.println("������� CharCode ��������� ����� ������� ������ ���������:");
                        String currencyChoice = scanner.nextLine();
                        if (bankAccount.getBalanceList().containsKey(currencyChoice.toUpperCase())) {
                            System.out.println("�� ����� ����� ������� ��������� ���� " + currencyChoice.toUpperCase() + "?");
                            int sum = Integer.parseInt(scanner.nextLine());
                            bankAccount.getBalanceList().get(currencyChoice.toUpperCase()).setMoney(sum + bankAccount.
                                    getBalanceList().get(currencyChoice.toUpperCase()).getMoney());
                            System.out.println(bankAccount.getName() + " �� ������� ��������� ���� �� " +
                                    sum + " " + currencyChoice.toUpperCase());
                        } else {
                            System.out.println("����� ������ ��� � ������.");
                        }
                    }
                } else if (choice == 3) {
                    bankAccount.CurrencyAccountCreate(course.getValute());
                } else if (choice == 4) {
                    converterList.startOfConverter(course.getValute(), bankAccount, course);
                } else if (choice == 5) {
                    bankAccount.getBalanceList().forEach((key, value) -> System.out.println(key + "=" + value.getMoney()));
                    System.out.println("������� CharCode ��������� ����� ������� ������ �������:");
                    String currencyChoice = scanner.nextLine();
                    if (bankAccount.getBalanceList().containsKey(currencyChoice.toUpperCase())) {
                        bankAccount.getBalanceList().remove(currencyChoice.toUpperCase());
                        System.out.println("�� ������� ������� �������� ���� " + currencyChoice.toUpperCase());

                    } else {
                        System.out.println("�������� ���� � " + currencyChoice.toUpperCase() + " �� ������.");
                    }


                } else if (choice == 6) {
                    bankAccount.getBalanceList().forEach((key, value) -> System.out.println(key + " ������: " + value.getMoney()));
                }


            } else {
                System.out.println("��� ������� ������������,������ �������������� ���?(��)");
                String unblock = scanner.nextLine();
                if (unblock.equals("��")) {
                    bankAccount.setLockAccount(false);
                } else {
                    System.out.println("�������� ����.");
                }
            }

        }
    }

}