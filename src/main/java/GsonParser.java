import com.google.gson.Gson;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

public class GsonParser {
    CurrencyList readUrl2() throws IOException {
        URL url = new URL("https://www.cbr-xml-daily.ru/daily_json.js");
        URLConnection con = url.openConnection();
        InputStream in = con.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        byte[] bytes = in.readAllBytes();
        String str = new String(bytes, StandardCharsets.UTF_8);
        Gson gson = new Gson();
        CurrencyList currencyList = gson.fromJson(str, CurrencyList.class);
        return currencyList;
    }
}



/*CurrencyList readUrl1() {
        Gson gson = new Gson();
        try (FileReader reader = new FileReader("gson.json")) {
            CurrencyList currencyList = gson.fromJson(reader, CurrencyList.class);
            return currencyList;

        } catch (Exception e) {
            System.out.println("Parsing error " + e.toString());

        }
        return null;
    }*/


