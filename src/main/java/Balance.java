public class Balance {
    private String currencyName;
    private double money;

    public Balance(double money) {
        this.money = money;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
