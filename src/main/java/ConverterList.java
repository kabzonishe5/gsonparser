import org.apache.commons.math3.util.Precision;

import java.util.*;

public class ConverterList {
    private List<Converter> converters = new ArrayList<>();

    public void startOfConverter(HashMap<String, Currency> valute, BankAccount bankAccount, CurrencyList currencyList) {
        System.out.println("����� ���������� � �������� ������ ����� ����� �������.");
        Scanner scanner = new Scanner(System.in);
        System.out.println("���� ������ �������� ������, ������� 1.");
        System.out.println("���� ������ ���������� ���� ������, ������� 2.");
        System.out.println("������� ������ ����� 3.");
        int number = Integer.parseInt(scanner.nextLine());
        if (number == 1) {
            System.out.println("������� CharCode ������ ������� ������ �������(�������� USD): ");
            bankAccount.getBalanceList().forEach((key, value) -> System.out.println(key.toUpperCase() + "=" + value.getMoney()));
            String charCodeBefore = scanner.nextLine();
            if (bankAccount.getBalanceList().containsKey(charCodeBefore.toUpperCase())) {
                System.out.println("������� CharCode ������ ������� ������ ������:");
                String charCodeAfter = scanner.nextLine();
                if (!valute.containsKey(charCodeAfter.toUpperCase())) {
                    System.out.println("����� ������ ���");
                }
                System.out.println("������� " + charCodeBefore.toUpperCase() + " ������ �� ������ ��������� � " + charCodeAfter.toUpperCase() + "?");
                int number1 = Integer.parseInt(scanner.nextLine());
                if (bankAccount.getBalanceList().containsKey(charCodeAfter.toUpperCase()) && valute.containsKey(charCodeAfter.toUpperCase())) {
                    if (number1 > bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).getMoney()) {
                        System.out.println("� ��� ��� " + number1 + " " + charCodeBefore.toUpperCase() + " �� �����.");
                    } else if (charCodeBefore.toUpperCase().equals("RUB") && number1 <= bankAccount.getBalanceList().
                            get(charCodeBefore.toUpperCase()).getMoney() && valute.containsKey(charCodeAfter.toUpperCase())) {
                        bankAccount.getBalanceList().get(charCodeAfter.toUpperCase()).setMoney
                                (Precision.round((number1 / currencyList.getValute().get(charCodeAfter.toUpperCase()).
                                        getValue()) + bankAccount.getBalanceList().get(charCodeAfter.toUpperCase()).getMoney(), 3));
                        bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).setMoney(Precision.round
                                (bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).getMoney() - number1, 3));
                    } else {
                        bankAccount.getBalanceList().get(charCodeAfter.toUpperCase()).setMoney(Precision.
                                round((number1 * valute.get(charCodeBefore.toUpperCase()).getValue()) / valute.
                                        get(charCodeAfter.toUpperCase()).getValue(), 3));
                        bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).setMoney
                                (Precision.round(bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).getMoney() - number1, 3));
                    }

                    double sum = Precision.round(number1, 3);
                    Date date = new Date();
                    int id = converters.size() + 1;
                    Converter converter = new Converter(id, sum, date, charCodeBefore, charCodeAfter);
                    converters.add(converter);
                    System.out.println("�� ������� �������� ������.");


                } else if (valute.containsKey(charCodeAfter.toUpperCase())) {
                    System.out.println("�� ����������� ��������� �� " + charCodeBefore.toUpperCase() + " � " + charCodeAfter.toUpperCase());
                    System.out.println("� ��� ��� " + charCodeAfter.toUpperCase() + " �����, ��� ������ �� ������������� ���������.");
                    if (charCodeBefore.toUpperCase().equals("RUB")) {
                        double sum = 0;
                        Balance balance = new Balance(sum);
                        bankAccount.getBalanceList().put(charCodeAfter.toUpperCase(), balance);
                        bankAccount.getBalanceList().get(charCodeAfter.toUpperCase()).setMoney
                                (Precision.round((number1 / currencyList.getValute().get(charCodeAfter.toUpperCase()).
                                        getValue()) + bankAccount.getBalanceList().get(charCodeAfter.toUpperCase()).getMoney(), 3));
                        bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).setMoney(Precision.round
                                (bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).getMoney() - number1, 3));
                        double sum1 = Precision.round(number1, 3);
                        Date date = new Date();
                        int id = converters.size() + 1;
                        Converter converter = new Converter(id, sum1, date, charCodeBefore, charCodeAfter);
                        converters.add(converter);
                        System.out.println("�� ������� �������� ������.");
                    } else {
                        double sum = 0;
                        Balance balance = new Balance(sum);
                        bankAccount.getBalanceList().put(charCodeAfter.toUpperCase(), balance);
                        bankAccount.getBalanceList().get(charCodeAfter.toUpperCase()).setMoney(Precision.
                                round((number1 * valute.get(charCodeBefore.toUpperCase()).getValue()) / valute.
                                        get(charCodeAfter.toUpperCase()).getValue(), 3));
                        bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).setMoney
                                (Precision.round(bankAccount.getBalanceList().get(charCodeBefore.toUpperCase()).getMoney() - number1, 3));
                        double sum1 = Precision.round(number1, 3);
                        Date date = new Date();
                        int id = converters.size() + 1;
                        Converter converter = new Converter(id, sum1, date, charCodeBefore, charCodeAfter);
                        converters.add(converter);
                        System.out.println("�� ������� �������� ������.");
                    }
                }
            } else {
                System.out.println("� ��� ��� " + charCodeBefore + " ��������� �����.");
            }
        } else if (number == 2) {
            System.out.println(valute);
        } else if (number == 3) {
            printList();
        } else {
            System.out.println("�������� ����");
        }

    }

    public void printList() {
        System.out.println(converters.toString());
    }
}
/*public void rubleConverter(HashMap<String, Currency> valute,BankAccount bankAccount) {
        System.out.println("������� CharCode ������ ������� ������ ������:");
        Scanner scanner = new Scanner(System.in);
        String charCodeAfter = scanner.nextLine();
        if (valute.containsKey(charCodeAfter.toUpperCase())) {
            System.out.println("������� ������ �� ������ ��������� � ������ " + charCodeAfter.toUpperCase());
            int number1 = Integer.parseInt(scanner.nextLine());
            double sum = Precision.round(number1 / valute.get(charCodeAfter.toUpperCase()).getValue(), 3);
            Date date = new Date();
            int id = converters.size() + 1;
            Converter converter = new Converter(id, sum, date, "RUB", charCodeAfter);
            converters.add(converter);
            System.out.println("�� ������� �������� ������.");
        } else {
            System.out.println("�� ����� �������� CharCode ������");
        }
    }*/