import com.google.gson.annotations.SerializedName;
import org.apache.commons.math3.util.Precision;

import java.util.HashMap;


public class CurrencyList {
    @SerializedName("Date")
    private String date;
    @SerializedName("PreviousDate")
    private String previousDate;
    @SerializedName("PreviousURL")
    private String previousURL;
    @SerializedName("Timestamp")
    private String timestamp;
    @SerializedName("Valute")
    private HashMap<String, Currency> valute;


    public HashMap<String, Currency> getValute() {
        return valute;
    }

    public void setValute(HashMap<String, Currency> valute) {
        this.valute = valute;
    }

    public void setCourse(HashMap<String, Currency> valute) {
        valute.forEach((key, value) -> value.setValue(Precision.round(value.getValue() / value.getNominal(), 3)));
    }


    public String getPreviousURL() {
        return previousURL;
    }

    public void setPreviousURL(String previousURL) {
        this.previousURL = previousURL;
    }

    public String getPreviousDate() {
        return previousDate;
    }

    public void setPreviousDate(String previousDate) {
        this.previousDate = previousDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return valute.toString() + "   " + date;
    }
}
