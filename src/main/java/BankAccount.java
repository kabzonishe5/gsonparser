import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class BankAccount {
    private String name;
    private String address;
    private HashMap<String, Balance> balanceList = new HashMap<>();
    private boolean LockAccount;
    private String number;

    public void createAccount() throws IOException {
        System.out.println("����������� �������.");
        Scanner scanner = new Scanner(System.in);
        System.out.println("������� ���� ��� � �������:");
        name = scanner.nextLine();
        System.out.println("������� ���� �����:");
        address = scanner.nextLine();
        System.out.println("������� ����� ��������(������ +7-***-***-**-**)");
        number = scanner.nextLine();
        while (number.length() != 16 && number.length() != 12) {
            System.out.println("�������� ������ ��������");
            System.out.println("���������� ���� ��� ���:");
            number = scanner.nextLine();
        }
        LockAccount = false;
        Balance balanceRUB = new Balance(0);
        balanceList.put("RUB", balanceRUB);

        System.out.println(name + ", �� ������� ������������������ ��� ������.");
        System.out.println("������� ��������� ���� �������� ����?(��/���)");
        String choice = scanner.nextLine();
        if (choice.equals("��")) {
            System.out.println("�������� ����� � ������:");
            double sum = Integer.parseInt(scanner.nextLine());
            balanceList.get("RUB").setMoney(sum);
            System.out.println("�� ������� ��������� �������� ����.");
        } else if (choice.equals("���")) {
            System.out.println("������� ������� ������ �������� ���� � ��������� ���?(��/���)");
            String choice2 = scanner.nextLine();
            if (choice2.equals("��")) {
                GsonParser gsonParser = new GsonParser();
                CurrencyList course = gsonParser.readUrl2();
                course.setCourse(course.getValute());
                CurrencyAccountCreate(course.getValute());
            } else if (choice2.equals("���")) {
                System.out.println("�������� ��� ���, " + name);
            } else {
                System.out.println("�������� ����.");
            }
        } else {
            System.out.println("�������� ����.");
        }


    }

    public void CurrencyAccountCreate(HashMap<String, Currency> valute) {
        Scanner scanner = new Scanner(System.in);
        valute.forEach((key, value) -> System.out.println(key + "     " + value.getName()));
        System.out.println("�������� ������ �� ������ ������� ������ �������� � �������� �� CharCode(������:USD):");
        String currencyName = scanner.nextLine();
        if (valute.containsKey(currencyName.toUpperCase()) && !balanceList.containsKey(currencyName.toUpperCase())) {
            System.out.println("�� ������� �������� ���� " + currencyName.toUpperCase());
            System.out.println("������� ��������� ���?(��/���)");
            String test = scanner.nextLine();
            if (test.equals("��")) {
                System.out.println("������� ����� � " + currencyName + " �� ������� ������ ��������� ����.");
                int sum1 = Integer.parseInt(scanner.nextLine());
                Balance balance = new Balance(sum1);
                balanceList.put(currencyName.toUpperCase(), balance);
                System.out.println("�� ������� ������� ����� �������� ���� � ��������� " + sum1 + " " + currencyName.toUpperCase());


            } else if (test.equals("���")) {
                double sum1 = 0;
                Balance balance = new Balance(sum1);
                balanceList.put(currencyName.toUpperCase(), balance);
                System.out.println("�� ������� ������� ����� �������� ���� " + currencyName.toUpperCase() + " ,������ " + balance.getMoney());

            } else {
                System.out.println("�������� ����.");
            }


        } else if (balanceList.containsKey(currencyName.toUpperCase())) {
            System.out.println("����� �������� ���� ��� ����������.");
        } else if (currencyName.toUpperCase().equals("RUB") && !balanceList.containsKey(currencyName.toUpperCase())) {
            System.out.println("�� ������� �������� ���� " + currencyName.toUpperCase());
            System.out.println("������� ��������� ���?(��/���)");
            String test = scanner.nextLine();
            if (test.equals("��")) {
                System.out.println("������� ����� � " + currencyName + " �� ������� ������ ��������� ����.");
                int sum1 = Integer.parseInt(scanner.nextLine());
                Balance balance = new Balance(sum1);
                balanceList.put(currencyName.toUpperCase(), balance);
                System.out.println("�� ������� ������� ����� �������� ���� � ��������� " + sum1 + " " + currencyName.toUpperCase());


            } else if (test.equals("���")) {
                double sum1 = 0;
                Balance balance = new Balance(sum1);
                balanceList.put(currencyName.toUpperCase(), balance);
                System.out.println("�� ������� ������� ����� �������� ���� " + currencyName.toUpperCase() + " ,������ " + balance.getMoney());

            } else if (!valute.containsKey(currencyName.toUpperCase())) {
                System.out.println("�������� ����");
            } else {
                System.out.println("�������� ����.");
            }

        }


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public HashMap<String, Balance> getBalanceList() {
        return balanceList;
    }

    public void setBalanceList(HashMap<String, Balance> balanceList) {
        this.balanceList = balanceList;
    }

    public boolean isLockAccount() {
        return LockAccount;
    }

    public void setLockAccount(boolean lockAccount) {
        LockAccount = lockAccount;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
